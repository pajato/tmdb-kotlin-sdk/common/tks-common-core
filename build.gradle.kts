plugins { alias(libs.plugins.kmp.lib) }

group = "com.pajato.tks"
version = "0.10.6"
description = "The TMDb Kotlin SDK (tks) common feature, core layer, KMP common target project"

kotlin.sourceSets {
    val commonMain by getting { dependencies { implementation(libs.kotlinx.serialization.json) } }

    val commonTest by getting {
        dependencies {
            implementation(libs.kotlin.test)
            implementation(libs.pajato.test)
        }
    }
}
