# tks-common-core

## Description

The Movie Database (TMDb) common feature, entities (core) layer, Kotlin MultiPlatform (KMP) common project providing
Android, iOS and Desktop targets.

This project defines TMDb common interfaces and data classes in the innermost Clean Architecture layer. It exists to
specify these artifacts which are used by other TMDb interfaces and by outer layers to provide data acquired from the
TMDb database.

## License

GPL, Version 3.0.  See the peer document LICENSE for details.

## Contributions

See the [contributing guide](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/CONTRIBUTING.md) in the
[Argus Documentation Project](https://gitlab.com/pajato/argus/argus-doc/-/blob/main/README.md).

## Project status

Converted to Kotlin Multiplatform (KMP) with versions 0.10.*

## Documentation

For general documentation on the TMDb API, see
[The Movie Database API](https://developers.themoviedb.org/3/getting-started/introduction).

As documentation entered into code files grows stale seconds after it is written, no such documentation is created.
Instead, documentation is created by you on demand using the Dokka Gradle task: 'dokkaGfm'. After successful task
completion, see the detailed documentation [here](build/dokka/gfm/index.md)

## Usage

To use the project, follow these steps:

1. Add the project as a dependency in your build file.
2. Import the necessary classes and interfaces from the project.
3. Use the provided APIs to interact with the shelf feature.

## Test Cases

The table below identifies the adapter layer unit tests. A test file name is always of the form `NamePrefixUnitTest.kt`.
The test file content is one or more test cases (functions)

| Filename Prefix   | Test Case Name                                                 |
|-------------------|----------------------------------------------------------------|
| Common            | When creating a default common object, verify behavior         |
|                   | When deserializing a null backdrop path, verify behavior       |
| Company           | When creating a default company object, verify behavior        |
| Country           | When creating a default country object, verify behavior        |
| CreatedBy         | When deserializing a list of creators, verify the correct size |
| Creator           | When creating a default creator object, verify behavior        |
| EpisodeKey        | When creating a default episode key, verify behavior           |
| Genre             | When creating a default genre object, verify behavior          |
|                   | When creating a genre, verify behavior                         |
| InjectError       | When creating an injection error exception, verify behavior    |
| MovieKey          | When creating a movie key, verify behavior                     |
| PersonKey         | When creating a person key, verify behavior                    |
| SearchKey         | When creating a default search key, verify behavior            |
| SeasonKey         | When creating a default season key, verify behavior            |
| SpokenLanguageKey | When creating a default spoken language, verify behavior       |
| TmdbKey           | Verify the names of the TMDb keys                              |
|                   | Verify the values of the KeyType enum class                    |
| TvKey             | When creating a default tv show key, verify behavior           |
| VideoStatus       | Exercise each enum name for code coverage                      |
|                   | Verify the values of the VideoStatus enum class                |

### Overview

### Notes

The single responsibility for this project is to provide the TMDb common artifact definitions used by this
and outer architectural layers. The adapter layer implements these core common interfaces.
