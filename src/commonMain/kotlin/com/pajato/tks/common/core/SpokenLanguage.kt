package com.pajato.tks.common.core

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public data class SpokenLanguage(
    @SerialName("english_name")
    val englishName: String = "",
    @SerialName("iso_639_1")
    val iso6391: String = "",
    val name: String = "",
)
