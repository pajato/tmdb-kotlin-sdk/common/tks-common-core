package com.pajato.tks.common.core

public const val POSTER_PATH: String = "poster_path"
public const val BACKDROP_PATH: String = "backdrop_path"
public const val GENRE_IDS: String = "genre_ids"
public const val LOGO_PATH: String = "logo_path"
public const val MEDIA_TYPE: String = "media_type"
public const val ORIGINAL_LANGUAGE: String = "original_language"
public const val ORIGINAL_TITLE: String = "original_title"
public const val RELEASE_DATE: String = "release_date"
public const val VOTE_AVERAGE: String = "vote_average"

public typealias TmdbId = Int
public typealias UrlFetcher = suspend (String) -> String
public typealias ErrorHandler = (String, Exception?) -> Unit
public typealias UriEncodedString = String
