package com.pajato.tks.common.core

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public data class Creator(
    @SerialName("credit_id")
    val creditId: String = "",
    val id: TmdbId = 0,
    val name: String = "",
    val gender: Int = 0,
    @SerialName("profile_path")
    val profilePath: String = "",
)
