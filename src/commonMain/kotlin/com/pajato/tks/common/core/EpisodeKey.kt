package com.pajato.tks.common.core

public data class EpisodeKey(val id: TmdbId = 0, val season: Int = 0, val episode: Int = 0) : TmdbKey()
