package com.pajato.tks.common.core

public data class PagingKey(val page: Int, val day: Int) : TmdbKey()
