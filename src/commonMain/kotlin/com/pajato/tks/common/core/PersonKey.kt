package com.pajato.tks.common.core

public data class PersonKey(val id: TmdbId) : TmdbKey()
