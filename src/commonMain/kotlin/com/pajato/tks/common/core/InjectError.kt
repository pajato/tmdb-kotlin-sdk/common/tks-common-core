package com.pajato.tks.common.core

internal const val ERROR_MESSAGE = "No repo implementation has been injected!"

public class InjectError(repo: String) : Throwable("$repo: $ERROR_MESSAGE")
