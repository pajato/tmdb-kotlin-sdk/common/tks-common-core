package com.pajato.tks.common.core

public data class TvKey(val id: TmdbId = 0) : TmdbKey()
