
package com.pajato.tks.common.core

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public data class Country(
    @SerialName("iso_3166_1")
    val iso31661: String = "",
    val name: String = "",
)
