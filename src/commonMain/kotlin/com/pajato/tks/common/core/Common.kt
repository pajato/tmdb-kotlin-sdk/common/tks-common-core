package com.pajato.tks.common.core

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public data class Common(
    val adult: Boolean = false,
    @SerialName("backdrop_path")
    val backdropPath: String = "",
    @SerialName("created_by")
    val createdBy: List<Creator> = listOf(),
    val genres: List<Genre> = listOf(),
    val homepage: String = "",
    val id: TmdbId = 0,
    // IMDB id pattern == ^tt[0-9]{7}, e.g. a 9 character string
    @SerialName("imdb_id")
    val imdbId: String = "",
    @SerialName("original_language")
    val originalLanguage: String = "",
    val overview: String = "",
    val popularity: Double = 0.0,
    @SerialName("poster_path")
    val posterPath: String = "",
    @SerialName("production_companies")
    val productionCompanies: List<Company> = listOf(),
    @SerialName("production_countries")
    val productionCountries: List<Country> = listOf(),
    @SerialName("spoken_languages")
    val spokenLanguages: List<SpokenLanguage> = listOf(),
    val status: String = "",
    val tagline: String = "",
    @SerialName("vote_average")
    val voteAverage: Double = 0.0,
    @SerialName("vote_count")
    val voteCount: Int = 0,
)
