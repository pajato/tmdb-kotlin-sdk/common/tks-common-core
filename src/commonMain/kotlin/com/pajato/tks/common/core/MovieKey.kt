package com.pajato.tks.common.core

public data class MovieKey(val id: TmdbId) : TmdbKey()
