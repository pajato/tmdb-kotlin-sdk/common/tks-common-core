package com.pajato.tks.common.core

public data class SearchKey(val type: String = "", val query: UriEncodedString = "", val page: Int = 0) : TmdbKey()
