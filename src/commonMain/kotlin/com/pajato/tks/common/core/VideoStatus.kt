package com.pajato.tks.common.core

public enum class VideoStatus { Rumored, Planned, InProduction, PostProduction, Released, Canceled, Ended }
