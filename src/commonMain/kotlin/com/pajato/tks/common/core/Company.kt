package com.pajato.tks.common.core

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable public data class Company(
    val description: String = "",
    val headquarters: String = "",
    val homepage: String = "",
    val id: TmdbId = 0,
    @SerialName("logo_path")
    val logoPath: String = "",
    val name: String = "",
    @SerialName("origin_country")
    val originCountry: String = "",
    @SerialName("parent_company")
    val parentCompany: String = "",
)
