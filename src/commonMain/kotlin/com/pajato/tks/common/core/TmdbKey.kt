package com.pajato.tks.common.core

public sealed class TmdbKey {
    public enum class KeyType { Episode, Season, Tv, Movie, Paging, Person, Search }

    public fun getName(): String = when (this) {
        is EpisodeKey -> KeyType.Episode.name
        is SeasonKey -> KeyType.Season.name
        is TvKey -> KeyType.Tv.name
        is MovieKey -> KeyType.Movie.name
        is PagingKey -> KeyType.Paging.name
        is PersonKey -> KeyType.Person.name
        is SearchKey -> "${KeyType.Search.name}${this.type}"
    }
}
