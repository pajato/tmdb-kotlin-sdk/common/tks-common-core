package com.pajato.tks.common.core

import kotlinx.serialization.Serializable

@Serializable public data class Genre(val id: TmdbId = 0, val name: String = "")
