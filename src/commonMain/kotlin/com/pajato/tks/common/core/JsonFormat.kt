package com.pajato.tks.common.core

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json

@OptIn(ExperimentalSerializationApi::class)
public val jsonFormat: Json = Json {
    isLenient = true
    coerceInputValues = true
    explicitNulls = false
    ignoreUnknownKeys = true
}
