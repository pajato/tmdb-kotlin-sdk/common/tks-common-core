package com.pajato.tks.common.core

public data class SeasonKey(val id: TmdbId = 0, val season: Int = 0) : TmdbKey()
