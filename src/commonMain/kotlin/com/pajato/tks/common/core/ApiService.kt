package com.pajato.tks.common.core

public interface ApiService {
    public fun inject(apiKey: String, urlFetcher: UrlFetcher, errorHandler: ErrorHandler)
}
