package com.pajato.tks.common.core

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class SearchKeyUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a default search key, verify behavior`() {
        val key = SearchKey()
        assertEquals("", key.type)
        assertEquals("", key.query)
        assertEquals(0, key.page)
    }
}
