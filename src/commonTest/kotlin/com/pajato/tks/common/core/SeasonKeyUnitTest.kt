package com.pajato.tks.common.core

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class SeasonKeyUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a default season key, verify behavior`() {
        val key = SeasonKey()
        assertEquals(0, key.id)
    }
}
