package com.pajato.tks.common.core

import com.pajato.test.ReportingTestProfiler
import kotlinx.serialization.encodeToString
import kotlin.test.Test
import kotlin.test.assertEquals

class GenreUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a default genre object, verify behavior`() {
        val genre = Genre()
        assertEquals(0, genre.id)
    }

    @Test fun `When creating a genre, verify behavior`() {
        val name = "War"
        val genre = Genre(0, name)
        assertEquals(0, genre.id)
        assertEquals(name, genre.name)
    }

    @Test fun `When a test genre object is serialized and deserialized, verify behavior`() {
        val genre = Genre()
        val json = jsonFormat.encodeToString(genre)
        assertEquals("{}", json)
        assertEquals(0, jsonFormat.decodeFromString<Company>(json).id)
    }
}
