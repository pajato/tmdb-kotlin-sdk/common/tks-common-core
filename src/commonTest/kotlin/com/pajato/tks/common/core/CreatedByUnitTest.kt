package com.pajato.tks.common.core

import com.pajato.test.ReportingTestProfiler
import kotlinx.serialization.builtins.ListSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

internal class CreatedByUnitTest : ReportingTestProfiler() {
    @Test fun `When deserializing a list of creators, verify the correct size`() {
        val name = "torchwood_created_by.json"
        val json = this.javaClass.classLoader.getResource(name)?.readText() ?: fail("Could not get resource!")
        val creatorList: List<Creator> = jsonFormat.decodeFromString(ListSerializer(Creator.serializer()), json)
        assertEquals(1, creatorList.size)
    }
}
