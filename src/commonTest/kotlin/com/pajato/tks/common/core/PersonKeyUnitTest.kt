package com.pajato.tks.common.core

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class PersonKeyUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a person key, verify behavior`() {
        val key = PersonKey(0)
        assertEquals(0, key.id)
    }
}
