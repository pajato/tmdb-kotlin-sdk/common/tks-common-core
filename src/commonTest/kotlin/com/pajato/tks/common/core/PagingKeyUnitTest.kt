package com.pajato.tks.common.core

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class PagingKeyUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a paging key, verify behavior`() {
        val key = PagingKey(0, 0)
        assertEquals(0, key.page)
        assertEquals(0, key.day)
    }
}
