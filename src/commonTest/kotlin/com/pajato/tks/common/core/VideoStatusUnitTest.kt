package com.pajato.tks.common.core

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

internal class VideoStatusUnitTest : ReportingTestProfiler() {
    @Test fun `Exercise each enum name for code coverage`() {
        assertEquals("Canceled", VideoStatus.Canceled.name)
        assertEquals("InProduction", VideoStatus.InProduction.name)
        assertEquals("Planned", VideoStatus.Planned.name)
        assertEquals("PostProduction", VideoStatus.PostProduction.name)
        assertEquals("Released", VideoStatus.Released.name)
        assertEquals("Rumored", VideoStatus.Rumored.name)
        assertEquals("Ended", VideoStatus.Ended.name)
    }

    @Test fun `Verify the values of the VideoStatus enum class`() {
        assertEquals(VideoStatus.Canceled, VideoStatus.valueOf("Canceled"))
        assertEquals(VideoStatus.InProduction, VideoStatus.valueOf("InProduction"))
        assertEquals(VideoStatus.Planned, VideoStatus.valueOf("Planned"))
        assertEquals(VideoStatus.PostProduction, VideoStatus.valueOf("PostProduction"))
        assertEquals(VideoStatus.Released, VideoStatus.valueOf("Released"))
        assertEquals(VideoStatus.Rumored, VideoStatus.valueOf("Rumored"))
        assertEquals(VideoStatus.Ended, VideoStatus.valueOf("Ended"))
    }
}
