package com.pajato.tks.common.core

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class CreatorUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a default creator object, verify behavior`() {
        val creator = Creator()
        assertEquals(0, creator.id)
    }
}
