package com.pajato.tks.common.core

import kotlin.test.Test
import kotlin.test.assertEquals

class TmdbKeyUnitTest {
    @Test fun `Verify the names of the TMDb keys`() {
        assertEquals(TmdbKey.KeyType.Episode.name, EpisodeKey().getName())
        assertEquals(TmdbKey.KeyType.Season.name, SeasonKey().getName())
        assertEquals(TmdbKey.KeyType.Tv.name, TvKey().getName())
        assertEquals(TmdbKey.KeyType.Movie.name, MovieKey(0).getName())
        assertEquals(TmdbKey.KeyType.Paging.name, PagingKey(0, 0).getName())
        assertEquals(TmdbKey.KeyType.Person.name, PersonKey(0).getName())
        assertEquals(TmdbKey.KeyType.Search.name, SearchKey().getName())
    }

    @Test fun `Verify the values of the KeyType enum class`() {
        assertEquals(TmdbKey.KeyType.Episode, TmdbKey.KeyType.valueOf("Episode"))
        assertEquals(TmdbKey.KeyType.Season, TmdbKey.KeyType.valueOf("Season"))
        assertEquals(TmdbKey.KeyType.Tv, TmdbKey.KeyType.valueOf("Tv"))
        assertEquals(TmdbKey.KeyType.Movie, TmdbKey.KeyType.valueOf("Movie"))
        assertEquals(TmdbKey.KeyType.Paging, TmdbKey.KeyType.valueOf("Paging"))
        assertEquals(TmdbKey.KeyType.Person, TmdbKey.KeyType.valueOf("Person"))
        assertEquals(TmdbKey.KeyType.Search, TmdbKey.KeyType.valueOf("Search"))
    }
}
