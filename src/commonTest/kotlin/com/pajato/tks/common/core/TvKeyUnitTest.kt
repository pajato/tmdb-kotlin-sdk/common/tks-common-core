package com.pajato.tks.common.core

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class TvKeyUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a default tv show key, verify behavior`() {
        val key = TvKey()
        assertEquals(0, key.id)
    }
}
