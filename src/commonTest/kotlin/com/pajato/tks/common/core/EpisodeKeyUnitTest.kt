package com.pajato.tks.common.core

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class EpisodeKeyUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a default episode key, verify behavior`() {
        val key = EpisodeKey()
        assertEquals(0, key.id)
    }
}
