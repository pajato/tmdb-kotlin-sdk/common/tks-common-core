package com.pajato.tks.common.core

import com.pajato.test.ReportingTestProfiler
import kotlinx.serialization.encodeToString
import kotlin.test.Test
import kotlin.test.assertEquals

class CountryUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a default country object, verify behavior`() {
        val country = Country()
        assertEquals("", country.name)
    }

    @Test fun `When a test country object is serialized and deserialized, verify behavior`() {
        val country = Country()
        val json = jsonFormat.encodeToString(country)
        assertEquals("{}", json)
        assertEquals(0, jsonFormat.decodeFromString<Company>(json).id)
    }
}
