package com.pajato.tks.common.core

import com.pajato.test.ReportingTestProfiler
import kotlinx.serialization.encodeToString
import kotlin.test.Test
import kotlin.test.assertEquals

class CompanyUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a default company object, verify behavior`() {
        val company = Company()
        assertEquals(0, company.id)
    }

    @Test fun `When a test company object is serialized and deserialized, verify behavior`() {
        val company = Company()
        val json = jsonFormat.encodeToString(company)
        assertEquals("{}", json)
        assertEquals(0, jsonFormat.decodeFromString<Company>(json).id)
    }
}
