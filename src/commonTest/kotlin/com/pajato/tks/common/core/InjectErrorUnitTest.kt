package com.pajato.tks.common.core

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class InjectErrorUnitTest : ReportingTestProfiler() {
    @Test fun `When creating an injection error exception, verify behavior`() {
        val repo = "Some Repo"
        val expectedMessage = "$repo: $ERROR_MESSAGE"
        val exc = InjectError(repo)
        assertEquals(expectedMessage, exc.message)
    }
}
