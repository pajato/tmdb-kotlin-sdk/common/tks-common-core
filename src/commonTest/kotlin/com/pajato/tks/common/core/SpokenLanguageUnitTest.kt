package com.pajato.tks.common.core

import com.pajato.test.ReportingTestProfiler
import kotlinx.serialization.encodeToString
import kotlin.test.Test
import kotlin.test.assertEquals

class SpokenLanguageUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a default spoken language, verify behavior`() {
        val language = SpokenLanguage()
        assertEquals("", language.name)
    }

    @Test fun `When a test spoken language object is serialized and deserialized, verify behavior`() {
        val spokenLanguage = SpokenLanguage()
        val json = jsonFormat.encodeToString(spokenLanguage)
        assertEquals("{}", json)
        assertEquals(0, jsonFormat.decodeFromString<Company>(json).id)
    }
}
