package com.pajato.tks.common.core

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class CommonUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a default common object, verify behavior`() {
        val common = Common()
        assertEquals(0, common.id)
    }

    @Test fun `When deserializing a null backdrop path, verify behavior`() {
        val json = """{"backdrop_path":null}"""
        val common: Common = jsonFormat.decodeFromString(json)
        assertEquals("", common.backdropPath)
    }
}
