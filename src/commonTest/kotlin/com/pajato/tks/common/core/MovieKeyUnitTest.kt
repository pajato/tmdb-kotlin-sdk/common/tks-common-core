package com.pajato.tks.common.core

import com.pajato.test.ReportingTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class MovieKeyUnitTest : ReportingTestProfiler() {
    @Test fun `When creating a movie key, verify behavior`() {
        val key = MovieKey(0)
        assertEquals(0, key.id)
    }
}
